import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView} from 'react-native';
import Task from '../../components/Task';

export default function HomeScreen() {
  const [task, setTask] = useState();
  const [taskItems, setTaskItems] = useState([]);
  const handleAddTask = () => {
    setTaskItems([...taskItems, task])
    setTask("");
  }
  const onCompleteTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy);
  }

    return (
      <View style={styles.container}>
        <TextInput style={styles.input}
         placeholder={'Write a text'} 
         value={task}
          onChangeText={text => setTask(text)}
        />  
        <TouchableOpacity onPress={() => handleAddTask()} >
          <Text style={styles.buttonDesign}>Add Task</Text>
        </TouchableOpacity>
        <ScrollView style={styles.scroll}
         contentContainerStyle={{flexGrow:1}} 
         keyboardShouldPersistTaps='handled'
         >
          <View style={styles.items}>
            {
              taskItems.map((item, index) => {
                  return (
                      <TouchableOpacity key={index} onPress={() => onCompleteTask(index)}>
                          <Task text={item}/>
                      </TouchableOpacity>
                  )
               })
            }
          </View>       
        </ScrollView>
    </View>
    );
  }

  const styles=StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: '#E8EAED'
    },
    input:{
      fontSize: 15,
      padding: 10,
      marginTop: 15,
      marginHorizontal: 40,
      backgroundColor: '#FFF',
      borderRadius: 10,
      borderColor: '#C0C0C0',
      borderWidth: 1,
      width: '80%',
    },
    buttonDesign:{
      fontSize: 15,
      padding: 8,
      paddingLeft: 45,
      marginTop: 15,
      marginLeft: 130,
      width: 150,
      backgroundColor: 'green',
      borderRadius: 10,
      borderColor: '#C0C0C0',
      borderWidth: 1,
      marginTop: 10,
      opacity: 0.5
    },
    items:{
      marginTop: 30
    },
    scroll:{
    marginTop: 20,
    width: '100  %' 
    }
  });