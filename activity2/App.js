import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from './screens/Home/HomeScreen';
import SettingsScreen from './screens/Settings/SettingScreen';

const homeScreen = "Home";
const completedTask = "Completed Tasks";

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
      initialRouteName={homeScreen}
      screenOptions={({route}) =>({
        tabBarIcon: ({focused, color, size}) =>{
          let iconName;
          let routeName = route.name;

          if (routeName === homeScreen){
            iconName = focused ? 'home' : 'home-outline'
          }else if (routeName === completedTask){
            iconName = focused ? 'list' : 'list-outline'
          }

          return <Ionicons name={iconName} size={size} color={color} />

        }, 
      })}
      >
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Completed Tasks" component={SettingsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}