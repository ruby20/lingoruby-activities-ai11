import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Task = (props) => {

    return(
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        padding: 15,
        marginTop: 15,
        marginHorizontal: 20,
        backgroundColor: '#FFF',
        borderRadius: 10,
        borderColor: '#C0C0C0',
        width: '90%',
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    circle: {
        width: 15,
        height: 15,
        backgroundColor: 'green',
        borderRadius: 10,
        marginRight: 15,
        opacity: 0.5
    },
    itemText: {
        fontSize: 15,
        maxWidth: '80%'
    }
});

export default Task;