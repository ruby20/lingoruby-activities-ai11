import { StatusBar } from 'expo-status-bar';
import React from "react";
import { StyleSheet, TextInput, Button, Alert, Text, SafeAreaView } from "react-native";

export default function App(){
    const[text, setText] = React.useState('')
    return (        
        <SafeAreaView style={Styles.container}>
            <TextInput onChangeText={(text) => setText(text)}
                style={Styles.input}
                placeholder='Type here...   '
            />
            <Button title = 'Submit'
                color="black"
                onPress = {() => Alert.alert("Alert", "You just typed: " + text)}
            />
        </SafeAreaView>
    )
}

const Styles=StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        margin: 50,
    },
    input:{
        borderColor: "black",
        borderWidth: 1,
        width: 200,
        marginBottom: 20
    }    
})
