import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import Title from "../components/title";

const Result = ({ navigation, route }) => {
  const { score } = route.params;

  const resultBanner =
    score > 4
      ? "https://cdn3d.iconscout.com/3d/premium/thumb/winning-trophy-5293767-4431729.png"
      : "https://cdn3d.iconscout.com/3d/premium/thumb/mistake-5183451-4333179.png";

  return (
    <View style={styles.container}>
      <Title titleText="Result" />
      <Text style={styles.score}>{score}</Text>
      <View style={styles.bannerContainer}>
        <Image
          source={{ uri: resultBanner }}
          style={styles.banner}
          resizeMode="contain"
        />
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate("Home")}
        style={styles.button}
      >
        <Text style={styles.buttonText}>Go To Home</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Result;

const styles = StyleSheet.create({
  banner: {
    height: 300,
    width: 400,
  },
  bannerContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  button: {
    width: "100%",
    backgroundColor: "#00A896",
    padding: 16,
    borderRadius: 16,
    alignItems: "center",
    marginBottom: 30,
  },
  buttonText: {
    fontSize: 24,
    fontWeight: "600",
    color: "white",
  },
  container: {
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#028090",
    height: "100%",
  },
  score: {
    fontSize: 30,
    fontWeight: "500",
    alignSelf: "center",
    color: "white",
  },
});
