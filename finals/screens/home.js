import React from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image } from "react-native";
import Title from "../components/title";

const Home = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Title titleText="Quiz Time" />
      <View style={styles.bannerContainer}>
        <Image
          source={{
            uri: "https://cdni.iconscout.com/illustration/free/thumb/solved-the-problem-2080982-1751388.png",
          }}
          style={styles.banner}
          resizeMode="contain"
        />
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate("Quiz")}
        style={styles.button}
      >
        <Text style={styles.buttonText}>Start</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  banner: {
    height: 400,
    width: 400,
  },
  bannerContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  button: {
    width: "100%",
    backgroundColor: "#00A896",
    padding: 16,
    borderRadius: 16,
    alignItems: "center",
    marginBottom: 30,
  },
  buttonText: {
    fontSize: 24,
    fontWeight: "600",
    color: "white",
  },
  container: {
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#028090",
    height: "100%",
  },
});
