import * as React from "react";
import { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { styles } from "./styles";
import History from "./history";

export default function Calculator() {
  const buttons = [
    "AC",
    "DEL",
    "%",
    "/",
    "7",
    "8",
    "9",
    "*",
    "4",
    "5",
    "6",
    "-",
    "3",
    "2",
    "1",
    "+",
    ".",
    "0",
    "+/-",
    "=",
  ];

  const [currentNumber, setCurrentNumber] = useState("");
  const [lastNumber, setLastNumber] = useState("");

  function calculator() {
    const splitNumbers = currentNumber.split(" ");
    const firstNumber = parseFloat(splitNumbers[0]);
    const lastNumber = parseFloat(splitNumbers[2]);
    const operator = splitNumbers[1];

    switch (operator) {
      case "+":
        setCurrentNumber((firstNumber + lastNumber).toString());
        return;
      case "-":
        setCurrentNumber((firstNumber - lastNumber).toString());
        return;
      case "*":
        setCurrentNumber((firstNumber * lastNumber).toString());
        return;
      case "/":
        setCurrentNumber((firstNumber / lastNumber).toString());
        return;
    }
  }

  function handleInput(buttonPressed) {
    console.log(buttonPressed);
    if (
      (buttonPressed === "+") |
      (buttonPressed === "-") |
      (buttonPressed === "*") |
      (buttonPressed === "/")
    ) {
      setCurrentNumber(currentNumber + " " + buttonPressed + " ");
      return;
    }
    switch (buttonPressed) {
      case "DEL":
        setCurrentNumber(currentNumber.substring(0, currentNumber.length - 1));
        return;
      case "AC":
        setLastNumber("");
        setCurrentNumber("");
        return;
      case "=":
        setLastNumber(currentNumber + " = ");
        calculator();
        return;
    }

    setCurrentNumber(currentNumber + buttonPressed);
  }

  return (
    <View>
      <View style={styles.result}>
        <TouchableOpacity></TouchableOpacity>
        <Text style={styles.historyText}>{lastNumber}</Text>
        <Text style={styles.resultText}>{currentNumber}</Text>
      </View>
      <View style={styles.buttons}>
        {buttons.map((button) =>
          button === "=" ? (
            <TouchableOpacity
              onPress={() => handleInput(button)}
              key={button}
              style={[styles.button, { backgroundColor: "#F1F2F3" }]}
            >
              <Text style={styles.btnText}>{button}</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => handleInput(button)}
              key={button}
              style={[styles.button, { backgroundColor: "#F1F2F3" }]}
            >
              <Text style={styles.btnText}>{button}</Text>
            </TouchableOpacity>
          )
        )}
      </View>
    </View>
  );
}
