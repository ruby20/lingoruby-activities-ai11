import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  result: {
    backgroundColor: "gray,",
    width: "100%",
    minHeight: 350,
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  resultText: {
    margin: 10,
    fontSize: 50,
  },
  buttons: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "flex-end",
  },
  button: {
    borderColor: "black",
    borderWidth: 0.3,
    borderRadius: 100,
    minWidth: 90,
    minHeight: 90,
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    margin: 3,
  },
  btnText: {
    fontSize: 25,
  },
  historyText: {
    fontSize: 35,
    margin: 10,
  },
});
