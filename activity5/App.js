import * as React from "react";
import { View } from "react-native";
import Calculator from "./components/calculator";

export default function App() {
  return (
    <View>
      <Calculator />
    </View>
  );
}
