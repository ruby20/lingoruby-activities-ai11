import "react-native-gesture-handler";
import * as React from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { TouchableOpacity } from "react-native-gesture-handler";

function Screen1({ navigation }) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        flexDirection: "column",
        backgroundColor: "white",
      }}
    >
      <Image
        source={require("./assets/images/screen1.png")}
        style={styles.image}
      />
      <Text
        style={{
          fontSize: 28,
          color: "black",
          textAlign: "center",
          fontWeight: "bold",
          marginHorizontal: 60,
          marginBottom: "50%",
        }}
      >
        Why did i chose IT?
      </Text>
      <TouchableOpacity onPress={() => navigation.navigate("Screen2")}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Next</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

function Screen2({ navigation }) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        flexDirection: "column",
        backgroundColor: "white",
      }}
    >
      <Image
        source={require("./assets/images/screen2.png")}
        style={styles.image}
      />
      <Text style={styles.title}>Increases my productivity</Text>
      <Text style={styles.text}>
        When I'm coding, I'm completely immersed in it. As a result, when I
        begin my activities, I start with programming.
      </Text>
      <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Prev</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Screen3")}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Next</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

function Screen3({ navigation }) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        flexDirection: "column",
        backgroundColor: "white",
      }}
    >
      <Image
        source={require("./assets/images/screen3.png")}
        style={styles.image}
      />
      <Text style={styles.title}>Satisfying</Text>
      <Text style={styles.text}>
        When I finish a piece of code and know I've done it well, I feel a sense
        of accomplishment; it's one of the reasons I chose IT in the first
        place.
      </Text>
      <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Prev</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Screen4")}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Next</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

function Screen4({ navigation }) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        flexDirection: "column",
        backgroundColor: "white",
      }}
    >
      <Image
        source={require("./assets/images/screen4.png")}
        style={styles.image}
      />
      <Text style={styles.title}>Interesting</Text>
      <Text style={styles.text}>
        As technology advances, I see amazing apps, games, websites, etc. I'd
        like to learn how to do it as well.
      </Text>
      <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Prev</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Screen5")}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Next</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

function Screen5({ navigation }) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        flexDirection: "column",
        backgroundColor: "white",
      }}
    >
      <Image
        source={require("./assets/images/screen5.png")}
        style={styles.image}
      />
      <Text style={styles.title}>In Demand</Text>
      <Text style={styles.text}>
        This is a practical reason, in addition to my reasons. I need a job as
        soon as I finish college, which is why I want to excel at this.
      </Text>
      <TouchableOpacity onPress={() => navigation.navigate("Screen1")}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Home</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Screen1" component={Screen1} />
        <Stack.Screen name="Screen2" component={Screen2} />
        <Stack.Screen name="Screen3" component={Screen3} />
        <Stack.Screen name="Screen4" component={Screen4} />
        <Stack.Screen name="Screen5" component={Screen5} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  image: {
    height: "50%",
    resizeMode: "contain",
    alignSelf: "center",
    alignItems: "center",
    width: "85%",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 28,
    color: "black",
    textAlign: "center",
    fontWeight: "bold",
    marginHorizontal: 60,
    marginBottom: "10%",
  },
  text: {
    fontSize: 18,
    color: "gray",
    textAlign: "center",
    fontWeight: "normal",
    marginHorizontal: 60,
    marginBottom: "30%",
  },
  button: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    width: 100,
    backgroundColor: "white",
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "#6C63FF",
    marginBottom: 50,
    alignItems: "center",
    marginLeft: 25,
    marginRight: 25,
  },
  buttonText: {
    fontSize: 15,
    color: "black",
  },
  buttonWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
